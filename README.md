Build pico8 from docker container.

## Build docker image

You first need `pico8` and `pico8.dat` binary files from your release.

```bash
git clone https://framagit.org/pico-8/dockerfile.git
cd dockerfile
cp /path/to/pico8 /path/to/pico8.dat
docker build -t pico8:<version> .
```

## Build your pico8 game

```bash
docker run pico8 -v /path/to/project:/game project.p8 # You need to mount data volume ti get builded files from container
```
