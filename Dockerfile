FROM debian

WORKDIR /game

RUN apt-get update && apt-get upgrade
RUN apt-get install -y xvfb unzip

ADD pico8 /opt/pico8
ADD pico8.dat /opt/pico8.dat
ADD pico8.sh /opt/pico8.sh

ENTRYPOINT ["/bin/bash", "/opt/pico8.sh"]
